var mainState = {
    preload: function() {
        game.load.image('player' , 'assets/player.png');
        game.load.image('bullet' , 'assets/bullet.png');
        game.load.image('enemy' , 'assets/enemy.png');
        game.load.image('enemybullet' , 'assets/enemybullet.png');
        game.load.image('background' , 'assets/background.jpg');
        
        game.load.spritesheet('explosion', 'assets/explosion.png', 32, 32);
        game.load.spritesheet('button' , 'assets/exitbotton.png',50,50);
        game.load.spritesheet('pause' , 'assets/pausebotton.png',50,50);
        game.load.spritesheet('life' , 'assets/heart.png',150,50);
        
        game.load.audio('playershot' , ['assets/playershot.ogg']);
        game.load.audio('enemyshot' , ['assets/enemyshot.wav']);
    },
    create: function() {
        this.back = game.add.tileSprite(0, 0, 800, 600, 'background');
        game.physics.startSystem(Phaser.Physics.ARCADE);
        //player
        this.player = game.add.sprite(game.width-450 , game.height-80 , 'player');
        game.physics.arcade.enable(this.player);

        //player's bullet
        this.bulletPool = game.add.group();
        this.bulletPool.enableBody = true;
        this.bulletPool.createMultiple(5,'bullet');
        this.bulletPool.setAll('outOfBoundsKill' , true);
        this.bulletPool.setAll('checkWorldBounds' , true);
        this.nextbulletAt = 0;
        this.bulletDelay = 300;

        //enemy
        this.enemyPool = game.add.group();
        this.enemyPool.enableBody = true;
        this.enemyPool.createMultiple(6,'enemy');
        this.enemyPool.setAll('outOfBoundsKill' , true);
        this.enemyPool.setAll('checkWorldBounds' , true);
        this.nextenemyAt = 0;
        this.enemyDelay = 3000;

        //enemy's bullet
        this.ebulletPool = game.add.group();
        this.ebulletPool.enableBody = true;
        this.ebulletPool.createMultiple(30,'enemybullet');
        this.ebulletPool.setAll('outOfBoundsKill' , true);
        this.ebulletPool.setAll('checkWorldBounds' , true);
        this.nextebulletAt = 0;
        this.ebulletDelay = 1000;
        
        //keyboard
        this.cursor = game.input.keyboard.createCursorKeys();

        //exit and pause
        var exitbutton = game.add.button(700, 15, 'button', this.exit , 1, 1, 0);
        ispause = false;

        //score & life
        this.scoreLabel = game.add.text(20 , 20 , 'score : 0', {font: '30px Impact',fill: '#FFFFFF'});
        this.score = 0;
        this.note = game.add.text(game.width/2 , 20 , 'life :  ', {font: '30px Impact',fill: '#FFFFFF'});
        this.note.anchor.setTo(1,0);
        this.life = game.add.sprite(this.note.x , this.note.y , 'life');
        this.lastlife = 3;

        //game start
        this.calllevel = false;
        var nameLabel = game.add.text(0 , game.height/2-50 , 'Level 1' , {font: '100px Impact',fill: '#00FFFF'});
        var tween = game.add.tween(nameLabel);
        tween.to({x:game.width} , 1500);
        tween.start();

        //audio
        this.playerSound = game.add.audio('playershot');
        this.enemySound = game.add.audio('enemyshot');
        if(sett){
            this.playerSound.volume = PlayerVolume;
            this.enemySound.volume = EnemyVolume;
        }
    },
    update: function() {
        if(this.score >= 60){
            this.back.tilePosition.y += 5;
        }
        else {
            this.back.tilePosition.y += 3;
        }
        if(this.player.alive){
            this.movePlayer();
            game.physics.arcade.overlap(this.bulletPool, this.enemyPool,this.enemyHit, null, this);
            game.physics.arcade.overlap(this.player, this.enemyPool,this.playerHit, null, this);
            game.physics.arcade.overlap(this.player, this.ebulletPool,this.playerHit, null, this);
            //player's bullet
            if(this.nextbulletAt<game.time.now){
                this.nextbulletAt = game.time.now + this.bulletDelay;
                var bullet = this.bulletPool.getFirstExists(false);
                bullet.reset(this.player.x+40 , this.player.y-15);
                bullet.body.velocity.y = -450;
                this.playerSound.play();
                bullet.play('fly');
            }
            //generate enemy
            if(this.nextenemyAt<game.time.now){
                this.nextenemyAt = game.time.now + this.enemyDelay;
                var enemy = this.enemyPool.getFirstExists(false);
                enemy.reset(game.rnd.integerInRange(0, 700) , 0);
                enemy.body.velocity.y = 50;
                enemy.play('fly');
            }
            if(this.nextebulletAt<game.time.now){
                this.enemyPool.forEach(this.enemyattack , this);
            }
            if(this.score == 60){
                this.ebulletDelay = 600;
                if(this.calllevel == false){
                    var level3 = game.add.text(0 , game.height/2-50 , 'Level 3' , {font: '100px Impact',fill: '#00FFFF'});
                    var tween3 = game.add.tween(level3);
                    tween3.to({x:game.width} , 1500);
                    tween3.start();
                    this.calllevel = true;
                }
            }
            else if(this.score == 30){
                this.ebulletDelay = 800;
                this.enemyDelay = 2000;
                if(this.calllevel == false){
                    var level2 = game.add.text(0 , game.height/2-50 , 'Level 2' , {font: '100px Impact',fill: '#00FFFF'});
                    var tween2 = game.add.tween(level2);
                    tween2.to({x:game.width} , 1500);
                    tween2.start();
                    this.calllevel = true;
                }
            }
            else {
                this.calllevel = false;
            }
        }
        else{
            game.time.events.add(1000,function(){game.state.start('over');},this);
        }
    },
    movePlayer: function(){
        if (this.cursor.left.isDown && this.player.x>0 && !ispause){
            this.player.x -= 10;
        }
        else if (this.cursor.right.isDown && this.player.x<game.width-100 && !ispause){
            this.player.x += 10;
        }
    },
    enemyHit: function(bullet, enemy) {
        bullet.kill();
        enemy.kill();
      
        var explosion = game.add.sprite(enemy.x, enemy.y, 'explosion');
        explosion.scale.setTo(2 , 2);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);
        this.score += 5;
        this.scoreLabel.text = 'score : ' + this.score;
    },
    playerHit: function(player, enemy) {
        enemy.kill();
        var explosion = game.add.sprite(player.x+20, player.y, 'explosion');
        explosion.scale.setTo(2.9 , 2.8);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);
        this.lastlife--;
        if(this.lastlife == 2){
            this.life.frame = 1;
        }
        else if(this.lastlife == 1){
            this.life.frame = 2;
        }
        else {
            player.kill();
        }
    },
    enemyattack: function(enemy){
        if(enemy.alive){
            this.nextebulletAt = game.time.now + this.ebulletDelay;
            var bullet = this.ebulletPool.getFirstExists(false);
            bullet.reset(enemy.x+27 , enemy.y+50);
            if(this.score >= 60){
                bullet.body.velocity.y = 600;
            }
            else if(this.score >= 30){
                bullet.body.velocity.y = 500;
            }
            else {
                bullet.body.velocity.y = 400;
            }
            this.enemySound.play();
            bullet.play('fly');
        }
    },
    exit: function(){
        game.state.start('menu');
    }
};
game.state.add('main', mainState);
var ispause;