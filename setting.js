var settingState = {
    preload: function(){
        game.load.image('background' , 'assets/background.jpg');
        game.load.spritesheet('up' , 'assets/up.png' , 50 , 50);
        game.load.spritesheet('down' , 'assets/down.png' , 50 , 50);
        game.load.spritesheet('back' , 'assets/back.png' , 50 , 50);
        game.load.audio('playershot' , ['assets/playershot.ogg']);
        game.load.audio('enemyshot' , ['assets/enemyshot.wav']);
    },
    create: function(){
        this.back = game.add.tileSprite(0, 0, 800, 600, 'background');
        this.cursor = game.input.keyboard.createCursorKeys();
        
        var Title = game.add.text(game.width/2 , game.height/2-100 , 'Setting' , {font: '100px Impact',fill: '#00FFFF'});
        Title.anchor.setTo(0.5,0.5);
        var mes1 = game.add.text(game.width/4 , game.height/2 , "Player's Sound Volume : " , {font: '20px Impact',fill: '#FFFFFF'});
        var playerupbutton = game.add.button(game.width/2+70, game.height/2+10 , 'up', this.playerup , 1, 1, 0);
        playerupbutton.anchor.setTo(0.5,0.5);
        var playerdownbutton = game.add.button(game.width/2+130, game.height/2+10 , 'down', this.playerdown , 1, 1, 0);
        playerdownbutton.anchor.setTo(0.5,0.5);
        var mes2 = game.add.text(game.width/4 , game.height/2+50 , "Enemy's Sound Volume : " , {font: '20px Impact',fill: '#FFFFFF'});
        var enemyupbutton = game.add.button(game.width/2+70, game.height/2+60 , 'up', this.enemyup , 1, 1, 0);
        enemyupbutton.anchor.setTo(0.5,0.5);
        var enemydownbutton = game.add.button(game.width/2+130, game.height/2+60 , 'down', this.enemydown , 1, 1, 0);
        enemydownbutton.anchor.setTo(0.5,0.5);

        var backbutton = game.add.button(15, 15, 'back', this.exit , 1, 1, 0);

        PlayerVolume = 0.5;
        EnemyVolume = 0.5;
    },
    update: function(){
        this.back.tilePosition.y += 2;
    },
    playerup: function(){
        this.playerSound = game.add.audio('playershot');
        this.playerSound.volume = PlayerVolume;
        if(this.playerSound.volume != 1){
            this.playerSound.volume += 0.1;
            PlayerVolume = this.playerSound.volume;
            this.playerSound.play();
        }
        else {
            this.playerSound.play();
        }
    },
    playerdown: function(){
        this.playerSound = game.add.audio('playershot');
        this.playerSound.volume = PlayerVolume;
        if(this.playerSound.volume != 0){
            this.playerSound.volume -= 0.1;
            PlayerVolume = this.playerSound.volume;
            this.playerSound.play();
        }
        else {
            this.playerSound.play();
        }
    },
    enemyup: function(){
        this.enemySound = game.add.audio('enemyshot');
        this.enemySound.volume = EnemyVolume;
        if(this.enemySound.volume != 1){
            this.enemySound.volume += 0.1;
            EnemyVolume = this.enemySound.volume;
            this.enemySound.play();
        }
        else {
            this.enemySound.play();
        }
    },
    enemydown: function(){
        this.enemySound = game.add.audio('enemyshot');
        this.enemySound.volume = EnemyVolume;
        if(this.enemySound.volume != 0){
            this.enemySound.volume -= 0.1;
            EnemyVolume = this.enemySound.volume;
            this.enemySound.play();
        }
        else {
            this.enemySound.play();
        }
    },
    exit: function(){
        game.state.start('menu');
    }
}
game.state.add('setting', settingState);
var PlayerVolume , EnemyVolume;