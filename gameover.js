var gameover = {
    preload: function(){
        game.load.image('background' , 'assets/background.jpg');
        game.load.spritesheet('button' , 'assets/exitbotton.png',50,50);
    },
    create: function(){
        this.back = game.add.tileSprite(0, 0, 800, 600, 'background');
        this.cursor = game.input.keyboard.createCursorKeys();
        var title = game.add.text(game.width/2 , game.height/2 , 'Game Over' , {font: '100px Impact',fill: '#FF0000'});
        title.anchor.setTo(0.5,0.5);
        var message = game.add.text(game.width/2 , game.height/2+100 , 'Press Enter to restart' , {font: '20px Arial' , fill: '#FFFFFF'});
        message.anchor.setTo(0.5,0.5);
        var button = game.add.button(700, 15, 'button', this.exit , 1, 1, 0);
    },
    update: function(){
        this.back.tilePosition.y += 2;
        if (game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
            game.state.start('main');
        }
    },
    exit: function(){
        game.state.start('menu');
    }
};
game.state.add('over', gameover);