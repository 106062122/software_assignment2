# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Raiden
* Key functions (add/delete)
    1. Menu/setting sounds volume
    2. game playing
    3. gameover
    
* Other functions (add/delete)
    

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify|15%|ultimate skills->N ; level->Y |
|Animations|20%|Y|
|UI|5%|Y|
|sound|5%|Y|
|leaderboard|5%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Multi-player|5%~15%|N|
|Enhanced items|5%~15%|N|
|Boss|5%|N|

## Website Detail Description

# 作品網址：https://106062122.gitlab.io/software_assignment2/

# Components Description : 
1. Menu State
    - 按鍵盤Enter鍵即可進入遊戲畫面
    - 右上角的設定按鈕進入後可調整音效大小
3. Game State
    - 玩家本身是跟對向來的敵人一樣不斷發射子彈，使用方向鍵控制自己的飛機進行躲閃並盡可能地多擊落敵機，但玩家不可進行前後移動，只能左右躲閃。
    - 玩家發射子彈時的聲音和敵方發射子彈實的聲音不同，可清楚分辨，亦可分開調整音量。
    - 左上角有目前成績，若是成功擊落一架敵機即可獲得五分，達到30分即會直接進入下一關，敵方子彈射擊的頻率以及敵機出現的頻率會增加，若是成功達到60分即會進一步增大難度。
    - 若是被敵機子彈打中抑或是撞到敵機即會損失一條命，總共僅有三次死亡機會，目前剩餘的次數顯示在畫面中間上方，若是三次機會用完即會跳往遊戲結束畫面。
    - 若是在遊戲進行中有突發事故無法繼續遊戲，也可以由右上角的Home鍵回到Menu介面
5. GameOver
    - 遊戲結束可選擇按Enter鍵再玩一次或是按右上角的按鈕回到主畫面
