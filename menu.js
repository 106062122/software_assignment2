var menuState = {
    preload: function(){
        game.load.image('background' , 'assets/background.jpg');
        game.load.spritesheet('setting' , 'assets/setting.png' , 50 , 50);
    },
    create: function(){
        this.back = game.add.tileSprite(0, 0, 800, 600, 'background');
        this.cursor = game.input.keyboard.createCursorKeys();
        //title
        var nameLabel = game.add.text(game.width/2 , -50 , 'Raiden' , {font: '100px Impact',fill: '#00FFFF'});
        nameLabel.anchor.setTo(0.5,0.5);
        var tween = game.add.tween(nameLabel);
        tween.to({y:game.height/2} , 500);
        tween.start();
        //alert message
        var message = game.add.text(game.width/2 , game.height+20 , 'Please Press Enter to start' , {font: '20px Impact',fill: '#FFFFFF'});
        message.anchor.setTo(0.5,0.5);
        var tween_mes = game.add.tween(message);
        tween_mes.to({y:game.height/2+100} , 1000);
        tween_mes.start();
        //settingbutton
        var setting = game.add.button(700, 15, 'setting', this.set , 1, 1, 0);
    },
    update: function(){
        this.back.tilePosition.y += 2;
        if (game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
            game.state.start('main');
        }
    },
    set: function(){
        sett = true;
        game.state.start('setting');
    }
}
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');
game.state.add('menu', menuState);
game.state.start('menu');
var sett = false;